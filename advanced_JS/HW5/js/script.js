// Задание
// Получить список фильмов серии Звездные войны и вывести их на экран. По клику выводить под фильмом список персонажей, появляющихся в нем.

// Технические требования:

// Отправить AJAX запрос по адресу https://swapi.co/api/films/ и получить список всех фильмов серии Звездные войны.
// Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на страницу. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl). Под этими данными вывести кнопку с надписью Список персонажей.
// При клике на кнопку Список персонажей должны отправлятся AJAX-запросы на сервер для поисках всех персонажей саги, которые появлялись в этом фильме. API для получения каждого персонажа фильма можно получить из свойства characters ленты.
// Как только с сервера будет получена информация о всех персонажах какого-либо фильма, вывести эту информацию на страницу под кнопкой Список персонажей для этого фильма, а саму кнопку можно удалить.
// AJAX запрос нужно реализовать с помощью XMLHttpRequest.


// Необязательное задание продвинутой сложности

// Пока загружаются персонажи фильма, прокручивать над кнопкой Список персонажей анимацию загрузки. Анимацию можно использовать любую. Желательно найти вариант на чистом CSS без использования JavaScript.

const container = document.getElementById('container')
container.classList.add('container')
const wrapper = document.createElement('div')
wrapper.classList.add('wrapper')
container.append(wrapper)


const request = new XMLHttpRequest();
request.open('GET', 'https://swapi.co/api/films/');
request.responseType = 'json'
console.log(request.responseType);

request.send();

request.onload = function () {
  if (request.status !== 200) {
    console.log(`Error ${req.status}:${req.statusText}`);
  } else {
    const result = request.response.results
    console.log(result);
    for (let i = 0; i < result.length; i++) {
      const episode_title = result[i].title
      const episode_number = result[i].episode_id
      const episode_opening = result[i].opening_crawl
      const episode_characters = result[i].characters

      const title = document.createElement("h4")
      title.classList.add('title')
      title.innerText = episode_title
      wrapper.append(title)

      const number = document.createElement("h5")
      number.classList.add("number")
      number.innerText = `Episode ${episode_number}`
      wrapper.append(number)

      const crawl = document.createElement("p")
      crawl.classList.add("crawl")
      crawl.innerText = episode_opening
      wrapper.append(crawl)

      const btn = document.createElement('button')
      btn.classList.add('char-btn')
      btn.innerText = "Show characters!"
      wrapper.append(btn)

      // __________________________

      btn.addEventListener('click',(e)=>{
        
      })

    }
    
  }
}