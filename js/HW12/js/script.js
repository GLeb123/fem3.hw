// Технические требования:

// В папке banners лежит HTML код и папка с картинками.
// При запуске программы на экране должна отображаться первая картинка.
// Через 10 секунд вместо нее должна быть показана вторая картинка.
// Еще через 10 секунд - третья.
// Еще через 10 секунд - четвертая.
// После того, как покажутся все картинки - этот цикл должен начаться заново.
// При запуске программы где-то на экране должна появиться кнопка с надписью Прекратить.
// По нажатию на кнопку Прекратить цикл завершается, на экране остается показанной та картинка, которая была там при нажатии кнопки.
// Рядом с кнопкой Прекратить должна быть кнопка Возобновить показ, при нажатии которой цикл продолжается с той картинки, которая в данный момент показана на экране.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// 

let images = document.querySelectorAll('.infinity-slider img');
let current = 0;

function slider() {
  for (let i = 0; i < images.length; i++) {
    images[i].classList.add('opacity0');
  }
  images[current].classList.remove('opacity0');
  
  if(current + 1 == images.length){
    current = 0;
  }
  else {
    current++;
  }
}
// document.querySelector('.infinity-slider').onclick = slider;

let timer =  setInterval(slider, 2000);
document.getElementById('stop').onclick = function(){
  clearInterval(timer);
}

document.getElementById('play').onclick = function(){
  timer = setInterval(slider, 2000);
}




// реализация с кнопками вверх и вниз
// let images = document.querySelectorAll('.infinity-slider img');
// let current = 0;

// function slider() {
  //   for (let i = 0; i < images.length; i++) {
//     images[i].classList.add('opacity0');
//   }
//   images[current].classList.remove('opacity0');
  
// }
// document.querySelector('.btn-up').onclick = function(){
//   if(current - 1 == -1){
//     current = images.length - 1;
//   }
//   else {
//     current--;
//   }
//   slider()
// };
// document.querySelector('.btn-down').onclick = function(){
  
//   if(current + 1 == images.length){
//     current = 0;
//   }
//   else {
//     current++;
//   }
//   slider()
// }
// // реализация с кнопками вверх и вниз











