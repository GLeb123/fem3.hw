// Реализовать возможность смены цветовой темы сайта пользователем.

// Технические требования:

// Взять любое готовое домашнее задание по HTML/CSS.
// Добавить на макете кнопку "Сменить тему".
// При нажатии на кнопку - менять цветовую гамму сайта (цвета кнопок, фона и т.д.) на ваше усмотрение. При повтором нажатии - возвращать все как было изначально - как будто для страницы доступны две цветовых темы.
// Выбранная тема должна сохраняться и после перезагрузки страницы

// let container = document.getElementById('container')
let BtnContainer = document.getElementById('btn-container')
const themeBtn = document.createElement("input")
themeBtn.id = 'theme-btn'
themeBtn.className = "theme-btn"
themeBtn.type = 'button'
themeBtn.value = 'Сменить тему'
themeBtn.style = "width: 170px"
themeBtn.style.height = "40px"
themeBtn.style.borderRadius = "5px"
themeBtn.style.color = 'black'
themeBtn.style.background = 'rgb(170, 242, 252);'
BtnContainer.appendChild(themeBtn); // put it into the DOM


// (function checkTheme () {
//   if (localStorage.getItem('localTheme') !== null) {
//     const theme = localStorage.getItem('localTheme');
//     container.classList.add(theme)
//   }
// })()

themeBtn.addEventListener('click', () => {
  // let secondTheme = document.querySelector('.second-theme');
  // let container = document.getElementById('container')
  // let divs = document.querySelectorAll('.thematic')
  // console.log(divs);
  // alert('Проверка входа в условие')
  if (localStorage.getItem('localTheme') === null) {
    // alert('убираем класс second-theme там где он есть')
    document.querySelector('.container').classList.toggle("second-theme");
    // alert('Кладём в Local storage тему First')
    localStorage.setItem('localTheme', 'second-theme');
  } else {
    // alert('добавляем second-theme')
    document.querySelector('.container').classList.toggle("second-theme");
  //  alert('кладём в local storage тему Second')
    localStorage.clear('localTheme');
  }
});


// 1.если локал сторейдж пуст - нужно сделать тоггле second theme и записать тему
// 2.clear
// 3.



// //Добавляем или изменяем значение:
// localStorage.setItem('myKey', 'myValue'); //теперь у вас в localStorage хранится ключ "myKey" cо значением "myValue"

// //Выводим его в консоль:
// var localValue = localStorage.getItem('myKey');
// console.log(localValue); //"myValue"

// //удаляем:
// localStorage.removeItem("myKey");

// //очищаем все хранилище
// localStorage.clear()

// То же самое, только с квадратными скобками:

// localStorage["Ключ"] = "Значение" //установка значения
// localStorage["Ключ"] // Получение значения
// delete localStorage["Ключ"] // Удаление значения


// //создадим объект
// var obj = {
// 	item1: 1,
// 	item2: [123, "two", 3.0],
// 	item3:"hello"
// };

// var serialObj = JSON.stringify(obj); //сериализуем его

// localStorage.setItem("myKey", serialObj); //запишем его в хранилище по ключу "myKey"

// var returnObj = JSON.parse(localStorage.getItem("myKey")) //спарсим его обратно объект


// // проверить есть ли ещё место в локал стордж
// try {
//   localStorage.setItem('ключ', 'значение');
// } catch (e) {
//   if (e == QUOTA_EXCEEDED_ERR) {
//    alert('Превышен лимит');
//   }
// }