// Считать с помощью модального окна браузера два числа.
// Считать с помощью модального окна браузера математическую операцию, которую нужно совершить. Сюда может быть введено +, -, *, /.
// Создать функцию, в которую передать два значения и операцию.
// Вывести в консоль результат выполнения функции.


let firstNumber = prompt('Enter number One', '1'); //в переменную записывем первое число
while (isNaN(firstNumber) || firstNumber === '' || firstNumber === null) {
  firstNumber = prompt('Enter number One Again, please', '1')
} //Проверка!Циклом while.isNaN - метод проверяет является ли значение Not a number или нет.
// цикл While крутит тут значение по кругу.Если введённое значение является Not a number,т.е не числом
// или является пустой строкой или null,тогда по новой просит ввести число

let secondNumber = prompt('Enter number Two', '2');
while (isNaN(secondNumber) || secondNumber === '' || secondNumber === null) {
  secondNumber = prompt('Enter number Two Again, please', '2')
}
// та же проверка на второй ввод

let operation = prompt('Enter operation, please', '+');
while (operation !== '+' && operation !== '-' && operation !== '*' && operation !== '/' && operation !== '**' && operation !== 'sq') {
  operation = prompt('Enter operation again, please', '+')
}

// аналогичная проверка на возможные оперторы

switch (operation) {
  case '+':
    alert(+(firstNumber) + +(secondNumber));
    break;
  case '-':
    alert(firstNumber - secondNumber);
    break;
  case '*':
    alert(firstNumber * secondNumber);
    break;
  case '/':
    alert(firstNumber / secondNumber);
    break;
}

// конструкция switch-case.
// Конструкция switch заменяет собой сразу несколько if.
// Хороший способ сравнить выражение сразу с несколькими вариантами.


// констуркцию могла выглядеть и так,но switch - case.Выглядит гораздо изящней.
// break нужен для того чтобы прервать выполнение

// if (operation === '+') {
//   alert(+(firstNumber) + +(secondNUmber));
// } else if (operation === '-') {
//   alert(+(firstNumber) - +(secondNUmber));
// } else if (operation === '*') {
//   alert(+(firstNumber) * +(secondNUmber));
// } else if (operation === '/') {
//   alert(+(firstNumber) / +(secondNUmber));
// }

// switch (operation) {
//   case '+':
//     alert(+(firstNumber) + +(secondNumber));
//     break;
//   case '-':
//     alert(firstNumber - secondNumber);
//     break;
//   case '*':
//     alert(firstNumber * secondNumber);
//     break;
//   case '/':
//     alert(firstNumber / secondNumber);
//     break;
// }