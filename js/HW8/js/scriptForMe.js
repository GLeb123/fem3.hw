
// Задание
// Создать поле для ввода цены с валидацией.

// Технические требования:

// При загрузке страницы показать пользователю поле ввода (input) с надписью Price. Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:

// При фокусе на поле ввода - у него должна появиться рамка зеленого цвета. При потере фокуса она пропадает.
// Когда убран фокус с поля - его значение считывается, над полем создается span, в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}. Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
// При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода, обнуляется.
// Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под полем выводить фразу - Please enter correct price. span со значением при этом не создается.


// В папке img лежат примеры реализации поля ввода и создающегося span.

let input = document.getElementById('priceInput');
//1. Нашли вразметке элемент для ввода
//2. повесили на фокус бордер
input.onfocus = () => input.style.borderColor = "teal";
input.onblur = () => {
	if(!(+input.value < 0))
		input.style.borderColor = "";
}
//3. Когда объект теряет фоекус ввода,если значение меньше 0 убирать рамку
//4. создаём див,кнопочку,спан и пэшку
let div = document.createElement('div');
let span = document.createElement('span');
let clsBtn = document.createElement('button');

let err = document.createElement('p');
err.innerText = "Please enter correct price";
//5. Задаём в пешку иннертекст
//6.На инпут вешаем onchage,что означает что он будет загораться,когда контент меняется
// и если значение больше или равно 0,тогда пешку нужно убрать
// Выводим в спан сообщение текущая цена такая -то
// появляется button с ткестом 'Х'
// на кнопочку вешаем функцию,котора обнуляет значене инпута и его цвет 
input.onchange = () => {
	if(+input.value >= 0){
		err.remove();
		span.innerText = `Текущая цена: ${input.value}`;
		div.appendChild(span);
		clsBtn.innerText = 'X';
		clsBtn.onclick = () => {
			input.value = "";
			input.style.backgroundColor = "";
      clsBtn.parentElement.remove();
      // parent Element - возвращает родителя
		}
		div.appendChild(clsBtn);
		document.body.insertBefore(div, document.body.children[0]);
		input.style.backgroundColor = 'teal';
	}else{
		input.style.borderColor = "red";
		input.style.backgroundColor = "";
		document.body.appendChild(err);
		if(clsBtn.parentElement)
			clsBtn.parentElement.remove();
	}
}

