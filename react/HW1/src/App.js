import React, { Component } from "react";
import "./App.css";
import Button from "./component/Button/Button";
import Modal from "./component/Modal/Modal";

class App extends Component {
  state = {
    firstModal: false,
    secondModal: false,
    display: false
  };

  openFirstModal = () => {
    this.setState(prevState => ({
      firstModal: !prevState.firstModal,
      secondModal: false,
      display: true
    }));
    // console.log("openFirstModal", this.state);
  };

  openSecondModal = () => {
    this.setState(prevState => ({
      secondModal: !prevState.secondModal,
      firstModal: false,
      display: true
    }));
    // console.log("openSecondModal", this.state);
  };

  closeModal = event => {
    let target = event.target;
    if (target.tagName !== "DIV") {
      this.setState(state => {
        return {
          display: false
        };
      });
    }
  };

  render() {
    const { firstModal, secondModal, display } = this.state;
    return (
      <div className="App">
        { firstModal && display ? (
          <div>
            <Modal
              className="modalOverlay"
              id="firstModalId"
              header="Заголовок 1"
              action={this.closeModal}
            />
          </div>
        ) : null}
        { secondModal && display ? (
          <div>
            <Modal
              className="modalOverlayTwo"
              id="secondModalId"
              header="Заголовок 2"
              action={this.closeModal}
            />
          </div>
        ) : null}
        <div>
          <Button
            id="firstButId"
            text="First modal"
            action={this.openFirstModal}
          />
          <Button
            id="secondButId"
            text="Second modal"
            action={this.openSecondModal}
          />
        </div>
      </div>
    );
  }
}

export default App;
