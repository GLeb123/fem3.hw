import React, { Component } from "react";

export default class Button extends Component {
  render() {
    const { action } = this.props;
    const { text } = this.props;

    return (
      <div>
        <button onClick={action}>{text}</button>
      </div>
    );
  }
}
