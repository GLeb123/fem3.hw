import React, { Component } from "react";
import "./Modal.css";

export default class Modal extends Component {
 

  render() {
    const { header, className, action } = this.props;
    return (
      <div>
        <section className={className} onClick={action}>
          <div className="modalWindow">
            <div className="modalHeader">
              <div className="modalTitle"></div>
              <div className="modalBody">{header}</div>
              <div className="modalFooter">
                <button id="submitBtnId">Submit</button>
                <button id="cancelBtnId">Cancel</button>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
