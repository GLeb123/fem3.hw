import React, { Component } from "react";
import { Button } from 'reactstrap'


export default class ActiveButton extends Component {
  render() {
    const { onClick } = this.props;

    return (
      <div>
        <Button color='info' onClick={ onClick }>ADD TO CARD</Button>
      </div>
    );
  }
}
