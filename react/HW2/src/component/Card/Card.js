import React, { Component } from "react";
import ActiveButton from "../Button/Button";
import FavoriteIcon from "../Icons/FavoriteIcon";
import BascetIcon from "../Icons/BascetIcon";
import ActiveModal from "../Modal/Modal";
// import FontAwesome from 'react-fontawesome';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar, faTrash } from "@fortawesome/free-solid-svg-icons";
import {
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle
  // Button,
  // ButtonGroup
} from "reactstrap";

import "./Card.scss";

class Good extends Component {
  state = {
    modalOpen: false
  };

  addToCartTST() {
    console.log("added");
    this.setState(state => ({
      modalOpen: !state.modalOpen
    }));
    // localStorage.setItem('CART:','')
  }

  addToFavorite = event => {
    const { id } = this.props.item;
    console.log("AddToFavorite");
    // console.log(event.target);
    // add element to fav array
    // Узнать id карты
    console.log(id);
    console.log(this.props.item);
  };

  render() {
    const { name, price, photo, article, id, addFunc } = this.props.item;
    const { addToCart, favToggler, removeFav } = this.props;
    const { modalOpen } = this.state;

    return (
      <div>
        <div>
          <Card sm="6" xs="12" mt="2" id={ id } outline color="secondary">
            <CardImg src={ photo } className="card-image" alt="Card image cap" />
            <CardBody className="card-body">
              <CardTitle className="card-title">{name}</CardTitle>
              <CardSubtitle>
                { price } { id }
              </CardSubtitle>
              <CardText>{ article }</CardText>
              <ActiveButton
                onClick={() => this.addToCart()}
                buttonLabel="kkkk"
              />
              <div>
                {modalOpen ? (
                  <ActiveModal
                    isModalOpen={this.state.modalOpen}
                    buttonLabel="ADD TO CARD"
                    closeModal={() => this.addToCart()}
                  />
                ) : null}

                <FavoriteIcon onClick={addFunc} />
                <BascetIcon />
                <button
                  onClick={() => addToCart({ name, price, photo, article, id })}
                >
                  ADDTOCART
                </button>
                <button
                  onClick={() => favToggler({ name, price, photo, article, id })}
                >
                  ADDTOFAV
                </button>
                <button
                  onClick={() => removeFav({ name, price, photo, article, id })}
                >
                  RemoveFromFav
                </button>
                <FontAwesomeIcon type="button" icon={faTrash} />
                <FontAwesomeIcon type="button" icon={faStar} id={id} />
              </div>
            </CardBody>
          </Card>
        </div>
      </div>
    );
  }
}

export default Good;
