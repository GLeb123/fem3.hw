import React, { useState } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

const ActiveModal = props => {
  // console.log(stateChecker);
console.log(props);
  const { className,  isModalOpen, closeModal } = props;

  const [modal, setModal] = useState(isModalOpen);

  const toggle = () => setModal(!modal);

 

  return (
    <div>
      {/* <Button color="danger" onClick={toggle}> */}
        {/* {buttonLabel} */}
      {/* </Button> */}
      <Modal isOpen={modal} toggle={toggle, closeModal}  className={className}>
        <ModalHeader toggle={toggle, closeModal}>Modal title</ModalHeader>
        <ModalBody>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle, closeModal}>
            Do Something
          </Button>{" "}
          <Button color="secondary" onClick={toggle, closeModal} >
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default ActiveModal;
