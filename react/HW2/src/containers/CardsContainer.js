import React from "react";
import Good from "../component/Card/Card";
import { Container, CardColumns } from "reactstrap";

class CardsContainer extends React.Component {
  render() {
    const { items } = this.props
    const { addToCart, favToggler, removeFav } = this.props;
    return (
      <>
        <Container className="mt-5">
          <CardColumns>
          {items.map(item => (
              <Good key={item.article} item={item} addToCart={addToCart} favToggler={favToggler} removeFav ={removeFav} />
            ))}         
          </CardColumns>
        </Container>     
      </>
    );
  }
}

export default CardsContainer;









