import React, { Component } from "react";
import "./App.css";
import CardsContainer from "./containers/CardsContainer";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Link, NavLink } from "react-router-dom";
import Cart from "./pages/Cart";
import Favorites from './pages/Favorites'

let data = [
  {
    name: "first_good",
    price: "7 000$",
    photo:
      "https://www.newcartestdrive.com/wp-content/uploads/2014/10/15-x6m-hero.jpg",
    article: "12345",
    id: "1"
  },
  {
    name: "second_good",
    price: "1 000 000 $",
    photo: "https://picsum.photos/id/111/800/600",
    article: "123456",
    color: "blue",
    id: "2"
  },
  {
    name: "third_good",
    price: "130$",
    photo: "https://picsum.photos/id/250/800/600",
    article: "1234567",
    color: "green",
    id: "3"
  },
  {
    name: "fourth_good",
    price: "140$",
    photo: "https://picsum.photos/id/25/800/600",
    article: "12345678",
    color: "black",
    id: "4"
  },
  {
    name: "fifth_good",
    price: "140$",
    photo: "https://picsum.photos/id/251/800/600",
    article: "123456789",
    color: "pink",
    id: "5"
  },
  {
    name: "sixth_good",
    price: "140$",
    photo: "https://picsum.photos/id/252/800/600",
    article: "12345679",
    color: "white",
    id: "6"
  },
  {
    name: "seven_good",
    price: "140$",
    photo: "http://cdn.motorpage.ru/Photos/800/2E36.jpg",
    article: "12345679",
    color: "white",
    id: "6"
  },
];

let dataJSON = JSON.stringify(data);
data = JSON.parse(dataJSON);


class App extends Component {
  state = {
    items: [],
    favItems: [ {
      name: "sixth_good",
      price: "140$",
      photo: "https://picsum.photos/id/252/800/600",
      article: "12345679",
      color: "white",
      id: "6"
    }],
    baskedItems: []
  };

  componentDidMount() {
    if (localStorage.getItem("CART" || "FAV")) {
      this.setState({
        items: data,
        baskedItems: JSON.parse(localStorage.getItem("CART")),
        favItems: JSON.parse(localStorage.getItem("FAV"))
      });
    } else {
      this.setState({
        items: data,
        baskedItems: [
          {
            name: "first_good",
            price: "7 000$",
            photo:
              "https://www.newcartestdrive.com/wp-content/uploads/2014/10/15-x6m-hero.jpg",
            article: "12345",
            id: "1"
          }
        ]
      });
    }
  }

  addToCartHandle = product => {
    console.log("addToCartHandle!!!");
    console.log(this.state.baskedItems);
    console.log(product);
    this.setState(state => {
      let baskedItems = [...state.baskedItems, product];
      let JSONbaskedItems = JSON.stringify(baskedItems);
      localStorage.setItem("CART", JSONbaskedItems);
      return {
        baskedItems
      };
    });
  };

  favoritesToggler = product => {
    console.log("favorigtesToggle");
    this.setState(state => {
      let favItems = [...state.favItems, product];
      let JSONfavItems = JSON.stringify(favItems);
      localStorage.setItem("FAV", JSONfavItems);
      
      return {
        favItems
      }; 
    });
  }; 

  removeFromFav = product => {
    let favItems = this.state.favItems.filter(el => el.id !== product.id);
    console.log('renoveFromFav' + product);
    console.log(favItems);
    return{
      favItems
    };
  };

  
  render() {
    const { items, baskedItems, favItems } = this.state;
    return (
      <Router>
        <>
          <ul>
            <li>
              <NavLink to="/" exact activeStyle={{ color: "green" }}>
                Home
              </NavLink>
            </li>
            <li>
              <NavLink to="/cart" exact activeStyle={{ color: "green" }}>
                CART
              </NavLink>
            </li>
            <li>
              <NavLink to="/favorites" exact activeStyle={{ color: "green" }}>
                Favorites
              </NavLink>
            </li>
          </ul>

          <Route
            path="/"
            exact
            render={() => {
              return (
                <CardsContainer
                  items={items}
                  addToCart={this.addToCartHandle}
                  favToggler={this.favoritesToggler}
                  removeFav={this.removeFromFav}

                />
              );
            }}
          />
          <Route
            path="/cart"
            render={() => {
              return <Cart arr={baskedItems} />;
            }}
          />
            <Route
            path="/favorites"
            render={() => {
              return <Favorites 
                      arr={items}
                      />;
            }}
          />
        </>
      </Router>
    );
  }
}
export default App;
