import React, { Component } from "react";
import Good from "../component/Card/Card";
import { Container, CardColumns } from "reactstrap";
import { is } from "@babel/types";

class Favorites extends Component {

  render() {
    const { arr } = this.props;
    
    
    
    return (
      <>
        <Container className="mt-5">
          {/* <CardColumns>            
            {arr.map(item => (
              <Good key={item.article} item={item} />
            ))}
          </CardColumns> */}
       <CardColumns>                        
            {arr.map(item => (
              <Good key={item.article} item={item} />
            ))}
          </CardColumns>          
        </Container>
      </>
    );
  }
}

export default Favorites;